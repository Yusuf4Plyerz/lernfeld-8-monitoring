#Dieser Quellcode wurde implementiert vom Team IdoIT für das Lernfeld 8 - Daten Systemübergreifend darstellen
#Bei Interesse stellen Sie Ihre Anfragen im GitLab

import argparse, socket, time, os                   #Hier werden die benötigten Datenpakete importiert

obj=argparse.ArgumentParser()                                               #Hier werden die Parameter festgelegt welche die 
obj.add_argument("show" ,help="shows the specified information.", choices=["cpu", "ram", "disk","all"], nargs="?")
obj.add_argument("ip", type=str, nargs="?")
args=obj.parse_args()
if args.ip==None:
    args.ip=input("Please enter server ip:")

def askfordata():                                                   #Hier wird dei Funktion gestartet welche die Verbindung zum Socket Host anfragt
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:                
        s.connect((args.ip, int(65432)))
        s.sendall(b'asking for data')
        data = s.recv(1024)                                         #Hier werden die Daten mit denen vom Socket festgelegt
    return data
if args.show==None:                                                 #Sobald keine Parameter eingebgen wurden soll der Text angegeben werden
    args.show=input("Please choose: all, cpu, ram, disks\n")


#In diesem Abschnitt wird eine Schleife ebschreiben welche die DAten vom Server abruft
#Es werden die aktuell angemeldeten Benutzer abgerufen 
#Es wird die Konsole bereinigt
#Bei bestimmter Eingabe von Parametern werden bestimmte Systemwerte angezeigt
#Diese werden alle im bestimmten Format unter KAtegorien angezeigt

while True:                                                         
    try:
        data=eval(askfordata())
        users=[x for i,x in enumerate(data["users"])]
        os.system("cls")
        print("-"*40,f"Resources of server: {args.ip}","-"*40,end="\n\n")
        if args.show == "cpu" or args.show == "all":
            print("-"*40,"CPU Info","-"*40)
            cpuper,cpumin,cpumax,cpucurrent=data["cpu"]
            print(f"Cpu%: {cpuper}%, Cpumin: {cpumin}Mhz, Cpumax: {cpumax}Mhz, Cpucurrent: {cpucurrent}Mhz")
        if args.show=="ram" or args.show=="all":
            print("-"*40,"RAM Info","-"*40)
            ramtotal,ramused,ramavailable,rampercent=data["ram"]
            print(f"Total Ram: {ramtotal} Gb, Ram Usage: {ramused} Gb, Ram Available: {ramavailable} Gb, Ram%: {rampercent}%")
        if args.show=="disk" or args.show=="all":
            print("-"*40,"Disk Info","-"*40)
            diskletters,diskdata=data["disk"]
            diskused,diskavailable,disktotal,diskpercent=[i for i in diskdata]
            for x, diskletter in enumerate(diskletters):
                print(f"{diskletter} Total:{disktotal[x]} Gb, Available:{diskavailable[x]} Gb, Used:{diskused[x]} Gb, Percent:{diskpercent[x]}%")
        print("-"*40,"Logged on Users","-"*40)
        print(f"{', '.join(users)}")
        print("-"*40,"End of Resources","-"*40)
        time.sleep(0.5)
    except Exception as e:                                                      #Falls ein Fehler auftritt wird dieser hier geprintet und der Vorgang wird beendet
        print("failed to connect: \n" + str(e))
        break