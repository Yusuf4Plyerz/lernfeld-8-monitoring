#Dieser Quellcode wurde implementiert vom Team IdoIT für das Lernfeld 8 - Daten Systemübergreifend darstellen
#Bei Interesse stellen Sie Ihre Anfragen im GitLab


import Monitoringserver, configparser, datetime, smtplib             #Dieser Abschnitt importiert die benötigten Datenpakete 

config=configparser.ConfigParser()                                #In diesem Abschnitt werden die Login Informationen aus der INI datei gelesen 
config.read(".\\INI")
info=config["smtp"]
login=info
email = login["namesend"]
password = login["password"]
host = login["host"]
print("Testing server connection...")


try:                                             #Hier wird versucht eine SMTP Verbindung aufzubauen
    global smtpObj
    smtpObj = smtplib.SMTP(host)
    smtpObj.login(email, password)
    login=True
except Exception:
    login=False
global sendmail
sendmail=datetime.datetime.now()

#In diesem Abschnitt wird bei fehlgeschglagenem Login die SMTP Funktion deaktiviert,
#es wird bei bestimmten Grenzwertüberschreitungen jeweilige Maßnahmen (.Log Datei beschrieben oder E-Mail versandt)
def main(cpu,ram,disks):                                                                
    global sendmail
    global smtpObj
    if not login:
        print("Mail function disabled. Server connection not available.")
    now=datetime.datetime.now()
    users=Monitoringserver.getusers()
    if cpu.cpu>=50 and cpu.cpu<=75:
        print("CPU reached 50%")
        with open("log.txt", "a+") as f:
            f.write(f"On {datetime.datetime.now().strftime('%D at %H:%M:%S')}, the server reached a cpu usage of {cpu.cpu}.\n")
            for x in range(len(users)):
                f.write(f"currently logged on: {users[x][0]}\n")
    elif cpu.cpu>=75:
        print("WARNING, CPU reached 75%")
        if now>sendmail:
            if login:
                smtpObj.sendmail(info['namesend'],info['name'],f"Subject:CPU reached {cpu.cpu}.\n\nAutomatic email. Do not reply\nOn {datetime.datetime.now().strftime('%D at %H:%M:S')}, the server reached a cpu usage of {cpu.cpu}. This can cause damage long term. ")
                sendmail=now + datetime.timedelta(hours=24)
        with open("log.txt", "a+") as f:
            f.write(f"On {datetime.datetime.now().strftime('%D at %H:%M:%S')}, the server reached a cpu usage of {cpu.cpu}.\n")
            for x in range(len(users)):
                f.write(f"currently logged on: {users[x][0]}\n")