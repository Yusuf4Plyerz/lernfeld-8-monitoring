#Dies ist der finale Quellcode zum Auslesen der Systemwerte der Server mithilfe einer INI Datei und der Eingabe von Parametern für den Kunden. 
#Die Verbindung geschieht über einen Socket welcher eine Öffnung im Server darstellt
#Dieser Quellcode wurde implementiert vom Team IdoIT für das Lernfeld 8 - Daten Systemübergreifend darstellen
#Bei Interesse stellen Sie Ihre Anfragen im GitLab




import psutil                                                                       #Dieser Abschnitt importiert die benötigten Datenpakete#
import time                                                                                                                                #
import win32api                                                                                                                            #
import os                                                                                                                                  #
import Alert                                                                                                                               #
import argparse                                                                                                                            #
import configparser                                                                                                                        #
import threading                                                                                                                           #
import socket                                                                                                                              #

os.system("cls")                                                                    #Hier wird die Konsole bereinigt#
parser = configparser.ConfigParser()                                                #Hier wird die INI Datei ausgelesen#   
parser.read(".\\INI")                                                                                                  #
info = parser["socket"]                                                     
obj = argparse.ArgumentParser()
obj.add_argument("-server", help="activates server mode",
                 action="store_false", default=True)
args = obj.parse_args()


class socketconnection(threading.Thread):                                           #Hier wird die Socket Verbidnung hergestellt#
    def __init__(self):                                                                                                         #
        self.socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                                                        #
        self.socket1.bind((info["host"], int(info["port"])))                                                                    #
        self.socket1.listen(2)                                                                                                  #
        threading.Thread.__init__(self)                                             #Der Thread Server wird hier gestartet#     #
        self.daemon = True                                                                                                #     #
        self.name = "Server Thread"                                                                                       #     #
        self.start()                                                                                                      #     #

    def run(self):                                                                  #Hier wird die Funktion gestartet welche in einer Schleife an den verbunden Socket die auszulesenden Systemwerte in Form von Objekten zusendet
        while True:
            clientsock, addr = self.socket1.accept()
            while True:
                neuerCPU = CPU()
                neuerRAM = RAM()
                neuerdatenträger = Datenträger()
                users = [psutil.users()[i][0]
                         for i, x in enumerate(psutil.users())]
                send = str({"cpu": [neuerCPU.cpu, neuerCPU.cpumin, neuerCPU.cpumax, neuerCPU.cpucurrent],
                           "ram": [neuerRAM.total, neuerRAM.used, neuerRAM.available, neuerRAM.percent],
                            "disk": [neuerdatenträger.drives, [neuerdatenträger.used, neuerdatenträger.available, neuerdatenträger.total, neuerdatenträger.percent]],
                            "users": users}).encode("utf8")

                data = clientsock.recv(1024)
                if not data:
                    break
                clientsock.send(send)
            clientsock.close()


class Datenträger():                                                            #Hier wird die Klasse gestartet, welche die Datenträger in einer Liste abruft und diese dann in GB umwandelt   
    def __init__(self):
        self.drives = win32api.GetLogicalDriveStrings()
        self.drives = self.drives.split('\000')[:-1]
        self.drives = [x.replace("\\", "") for x in self.drives]
        self.used = []
        self.available = []
        self.total = []
        self.percent = []
        if isinstance(self.drives, list):
            for i, x in enumerate(self.drives):
                try:
                    self.used.append(gbconvert(psutil.disk_usage(x).used))
                    self.available.append(gbconvert(psutil.disk_usage(x).free))
                    self.total.append(gbconvert(psutil.disk_usage(x).total))
                    self.percent.append(gbconvert(psutil.disk_usage(x).percent))
                except:
                    self.drives.remove(x)
        else:
            self.used.append(gbconvert(psutil.disk_usage(x).used))
            self.available.append(gbconvert(psutil.disk_usage(x).free))
            self.total.append(gbconvert(psutil.disk_usage(x).total))
            self.percent.append(gbconvert(psutil.disk_usage(x).percent))

    def schreiben(self):                                                                                    #Hier wird die Funktion gestartet, welche die Datenträger in der Konsole im best. Format printet
        if isinstance(self.drives, list):
            liste=list(zip(self.drives,self.total,self.used,self.available,self.percent))
            for i,x in enumerate(liste):
                print(f"{x[0]}total: {x[1]}GB, used: {x[2]}GB, available: {x[3]}GB, percentage: {x[4]}%")
        else:
            print(f"{str(self.drives)} {str(self.werte[0])} GB")


class RAM():                                                                        #Hier wird die Klasse gestartet, welche die Werte des RAMs ausliest und diese in GB umwandelt
    def __init__(self):
        self.memory = psutil.virtual_memory()
        self.total = gbconvert(self.memory.total)
        self.available = gbconvert(self.memory.available)
        self.used = gbconvert(self.memory.used)
        self.percent = self.memory.percent

    def schreiben(self):                                                            #Hier wird die Funktion gestartet, welche die Werte des RAMs in der Konsole im best. Format printet
        print(
            f"total: {self.total} GB \nused:  {self.used} GB \navailable: {self.available} GB \npercent:{self.percent}%")

def gbconvert(data):
    return round(data/2**30)

class CPU():                                                                             #Hier wird die Klasse gestartet, welche die Werte des CPU ausliest
    def __init__(self):
        self.cpu = psutil.cpu_percent()
        self.cpumax = psutil.cpu_freq().max
        self.cpucurrent = psutil.cpu_freq().current
        self.cpumin = psutil.cpu_freq().min

    def schreiben(self):                                                              #Hier wird dei Funktion gestartet, welche die Werte des CPU im best. Format ausliest
        print(
            f"%usage: {self.cpu} \nCPU Frequency: {self.cpucurrent} \nCPU Freq. Max: {self.cpumax} \nCPU Freq. Min: {self.cpumin}")


def getusers():                                                             #Hier wird die Funktion gestartet, welche die aktuellen Benutzer ausliest
    return [psutil.users()[i] for i, x in enumerate(psutil.users())]


class resources(threading.Thread):                                          #Hier wird die Klasse gestartet, welche den Ressource Thread startet und die Systemwerte ausliest, indem Sie auf die obigen Klasse zurückgreift
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = "Resource Thread"
        self.daemon = True
        self.start()

    def run(self):
        while True:
            os.system("cls")
            self.users = getusers()
            self.users = [self.users[i][0] for i, x in enumerate(self.users)]
            print("-"*40,"CPU Info","-"*40)
            self.neuerCPU = CPU()
            self.neuerCPU.schreiben()
            print("-"*40,"RAM Info","-"*40)
            self.neuerRAM = RAM()
            self.neuerRAM.schreiben()
            print("-"*40,"Disk Info","-"*40)
            self.neuerdatenträger = Datenträger()
            self.neuerdatenträger.schreiben()
            print("-"*40,"Currently logged on users","-"*40)
            print(", ".join(self.users))
            print("-"*40, "End of Performance Data","-"*40)
            Alert.main(self.neuerCPU, self.neuerRAM, self.neuerdatenträger)             #Hier wird der Status von Alert ausgegeben
            time.sleep(0.5)


if __name__ == "__main__":                                                      #Hier wird das Skript gestartet welche die Threads ressources und server startet
    resourcethread = resources()
    threads = [resourcethread]
    if args.server:                                                         #Hier wird der server thread gestartet 
        serverthread = socketconnection()
        threads.append(serverthread)
    for thread in threads:
        thread.join()
